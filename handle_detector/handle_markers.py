#!/usr/bin/env python

import rospy
import signal
import numpy as np
import geometry_msgs
from threading import Thread
from handle_detector.msg import HandleListMsg
from visualization_msgs.msg import Marker, MarkerArray
import tf.transformations

class HandlesMakerPublisher(Thread) :

    def __init__(self):
        Thread.__init__(self)
        signal.signal(signal.SIGINT, signal.SIG_DFL)

        self.handle_markers = {}
        self.handle_marker_array = MarkerArray()

        self.handles_pub = rospy.Publisher("/handle_detector/handle_markers", MarkerArray)
        self.handles_sub = rospy.Subscriber("/handle_detector/handle_list", HandleListMsg, self.handles_callback)


    def handles_callback(self, data):
        if len(data.handles) > 0:
            self.ready = False
            print "received", str(len(data.handles)), "handles"
            self.create_handle_markers(data.handles, data.header.frame_id)

            self.handle_marker_array = MarkerArray()
            # print self.handle_markers
            for m in self.handle_markers.values() :
                self.handle_marker_array.markers.append(m)
            self.handles_pub.publish(self.handle_marker_array)

    def create_handle_markers(self, handles, frame) :
        self.handle_markers = {}
        i = 0
        for handle in handles :
            pose = self.calculate_mean_handle(frame, handle)
            self.handle_markers[i] = self.create_cylinder_marker(pose, i)
            i = i+1

    def create_cylinder_marker(self, pose, id):
        marker = Marker()
        marker.type = Marker.SPHERE
        marker.header.frame_id = pose.header.frame_id
        marker.action = Marker.ADD
        marker.color.a = 1.0
        marker.color.r = 1
        marker.color.g = 0
        marker.color.b = 0
        marker.scale.x = 0.05
        marker.scale.y = 0.15
        marker.scale.z = 0.05
        marker.pose = pose.pose
        marker.id = id
        return marker

    def calculate_mean_handle(self, sensor_frame, handle):
        x, y, z = 0.0, 0.0, 0.0
        avg = geometry_msgs.msg.PointStamped()
        avg.header.frame_id = sensor_frame
        avg.header.stamp = rospy.Time(0)

        count = len(handle.cylinders)
        for cyl in handle.cylinders:
            x += cyl.pose.position.x
            y += cyl.pose.position.y
            z += cyl.pose.position.z
        x = x / count
        y = y / count
        z = z / count
        avg.point.x = x
        avg.point.y = y
        avg.point.z = z

        dist = 1000.0
        closest = handle.cylinders[0]

        for cyl in handle.cylinders:
            distance = np.sqrt( pow((x - cyl.pose.position.x), 2) +
                                pow((y - cyl.pose.position.y), 2) +
                                pow((z - cyl.pose.position.z), 2) )
            if distance < dist:
                dist = distance
                closest = cyl
        
        vec3 = geometry_msgs.msg.Vector3Stamped()
        vec3.vector = closest.axis
        vec3.header = avg.header
        major = [vec3.vector.x, vec3.vector.y, vec3.vector.z]
        
        
        pose = geometry_msgs.msg.PoseStamped()

        # vector from affordance to world
        vector = tf.transformations.unit_vector([-closest.pose.position.x, -closest.pose.position.y, -closest.pose.position.z])
        # vector in direction of arm
        perp = np.cross(major, vector)
        # vector perpendicular to arm vector and major axis
        perp2 = np.cross(major, perp)
        
        M = np.array(((perp2[0], major[0], perp[0], 0), 
                      (perp2[1], major[1], perp[1], 0), 
                      (perp2[2], major[2], perp[2], 0), 
                      (0, 0, 0, 1)), dtype=np.float64)
        
        x, y, z, w = tf.transformations.quaternion_from_matrix(M)
        pose.header = avg.header
        pose.pose.position = closest.pose.position
        pose.pose.orientation.x = x
        pose.pose.orientation.y = y
        pose.pose.orientation.z = z
        pose.pose.orientation.w = w
        
        return pose

if __name__=="__main__":
    
    rospy.init_node("handle_marker_publisher")

    try:
        node = HandlesMakerPublisher()
        node.start()
    except rospy.ROSInterruptException:
        # TODO: should not pass
        pass
    
    rospy.spin()

