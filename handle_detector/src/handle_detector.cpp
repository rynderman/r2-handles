#include "handle_detector/affordances.h"
#include "handle_detector/CylinderArrayMsg.h"
#include "handle_detector/CylinderMsg.h"
#include "handle_detector/HandleListMsg.h"
#include "handle_detector/cylindrical_shell.h"
#include "handle_detector/messages.h"
#include "handle_detector/visualizer.h"
#include "handle_detector/FindHandles.h"

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sstream>
#include <stdlib.h> 
#include <stdio.h>
#include <string.h>
#include <tf/transform_listener.h>
#include <vector>
#include <iostream>
#include <ctype.h>

#include "Eigen/Dense"
#include "Eigen/Core"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/surface/gp3.h>
#include <pcl/features/feature.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <pcl/point_types.h>
#include <pcl/features/feature.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/surface/mls.h>

#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include <tf/transform_broadcaster.h>

 #include <std_srvs/Empty.h>


#define EIGEN_DONT_PARALLELIZE

const std::string RANGE_SENSOR_FRAME = "/r2/asus_frame";
const std::string RANGE_SENSOR_TOPIC = "/swissranger_camera/pointcloud2_raw";
const std::string WORKSPACE_FRAME = "/r2/pelvis";

typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloud;

// input and output ROS topic data
PointCloud::Ptr g_cloud(new PointCloud);
Affordances g_affordances;
std::vector<CylindricalShell> g_cylindrical_shells;
std::vector< std::vector<CylindricalShell> > g_handles2;
std::string g_sensor_frame;
std::string g_sensor_topic;
std::string g_workspace_frame;
pcl::PointXYZ around_point;

// synchronization
double g_prev_time = -100000000;
double g_update_interval;
bool g_has_read = false;
bool has_been_requested = false;
bool g_should_publish = false;

double scale = 1.0;
boost::shared_ptr<interactive_markers::InteractiveMarkerServer> server;



PointCloud::Ptr cleanCloud(const PointCloud::Ptr &cloud_in)
{
  ROS_INFO_STREAM("handle_detector: Cleaning, " << cloud_in->points.size() << " points to start out with");
  PointCloud::Ptr cloud_filtered(new PointCloud);

  if (cloud_in->points.size() > 50)
  {

    ROS_INFO("handle_detector: Removing outliers");

    pcl::PointCloud<PointT>::Ptr filtered (new pcl::PointCloud<PointT>);
    pcl::StatisticalOutlierRemoval<PointT> sor;
    sor.setInputCloud (cloud_in);
    sor.setMeanK (40);
    sor.setStddevMulThresh (2.0);
    sor.filter (*filtered);

    ROS_INFO("MLS smoothing");

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree5 (new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointNormal> mls_points;
    pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;

    mls.setUpsamplingMethod(pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal>::RANDOM_UNIFORM_DENSITY);
    mls.setPointDensity(60);
    mls.setComputeNormals (true);
    mls.setInputCloud (filtered);
    mls.setPolynomialFit (true);
    mls.setSearchMethod (tree5);
    mls.setSearchRadius (0.04);
    mls.process (mls_points);
    pcl::PointCloud<PointT>::Ptr mls_cloud (new pcl::PointCloud<PointT>);
    pcl::copyPointCloud(mls_points, *mls_cloud);

    cloud_filtered = mls_cloud;

    ROS_INFO_STREAM("handle_detector: Done cleaning, " << cloud_filtered->points.size() << " points remaining");
  }
  else
  {
    ROS_WARN("handle_detector: Not enough points in selected volume to clean");
  }
  return cloud_filtered;
}

void cloudCallback(const sensor_msgs::PointCloud2ConstPtr& input)
{
  if (g_has_read || !has_been_requested)
    return;

  ROS_INFO("handle_detector: Reading in pointcloud");

  // check whether input frame is equivalent to range sensor frame constant
  std::string input_frame = input->header.frame_id;
  // if (input_frame.compare(g_sensor_frame) != 0)
  // {
  //   printf("handle_detector: Input frame %s is not equivalent to output frame %s ! Exiting ...\n", input_frame.c_str(), g_sensor_frame.c_str());
  //   std::exit(EXIT_FAILURE);
  // }
  printf("handle_detector: input frame: %s, output frame: %s\n", input_frame.c_str(), g_sensor_frame.c_str());

  // convert ROS sensor message to PCL point cloud
  PointCloud::Ptr cloud(new PointCloud);
  pcl::fromROSMsg(*input, *cloud);

  tf::TransformListener transform_listener;
  PointCloud::Ptr cloud_t(new PointCloud);
  transform_listener.waitForTransform(input_frame, g_workspace_frame, ros::Time(0), ros::Duration(5.0));

  tf::StampedTransform transform;
  transform_listener.lookupTransform(g_workspace_frame, input_frame, ros::Time(0), transform);

  pcl_ros::transformPointCloud(*cloud, *cloud_t, transform);

  // stop listening for new clouds
  g_has_read = true;

  ROS_INFO_STREAM("handle_detector: input cloud has " << cloud_t->points.size() << " points");
  ROS_INFO("handle_detector: Trimming workspace to around %f %f %f", around_point.x , around_point.y, around_point.z);

  // TODO: instead of having workspace as a cube in front of the robot
  // allow the user to select from a choice of workspaces:
  // full-hemisphere, right-hem, left-hem, front-hem, front-cube
  // each would have an upper and lower height bound defined in pelvis frame
  g_cloud = g_affordances.workspaceFilter(cloud_t, around_point);
  g_cloud = cleanCloud(g_cloud);
  g_cloud->header.frame_id = g_workspace_frame;

  //  PointCloud::Ptr cloud_2(new PointCloud);
  //  transform_listener.waitForTransform("/pelvis", "/global", ros::Time(0), ros::Duration(5.0));
  //  pcl_ros::transformPointCloud("/global", *g_cloud, *cloud_2, transform_listener);

  //  g_cloud = cloud_2;

  if (g_cloud->points.size() > 10)
  {
    // search grasp affordances
    g_cylindrical_shells = g_affordances.searchAffordances(g_cloud);

    // search handles
    g_handles2 = g_affordances.searchHandles(g_cloud, g_cylindrical_shells);

    // store current time
    g_prev_time = omp_get_wtime();
  }
  else
  {
    ROS_WARN_STREAM("handle_detector: Not enough points to search for handles: " << g_cloud->points.size() );
  }

  // can listen for new request now
  has_been_requested = false;
}

visualization_msgs::Marker makeBox( visualization_msgs::InteractiveMarker &msg )
{
  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::CUBE;
  marker.scale.x = msg.scale*0.5;
  marker.scale.y = msg.scale*0.5;
  marker.scale.z = msg.scale*0.5;
  marker.color.r = 0.5;
  marker.color.g = 0.5;
  marker.color.b = 0.5;
  marker.color.a = 0.2;
  return marker;
}

visualization_msgs::InteractiveMarkerControl& makeBoxControl( visualization_msgs::InteractiveMarker &msg )
{
  visualization_msgs::InteractiveMarkerControl control;
  control.always_visible = true;
  control.markers.push_back( makeBox(msg) );
  msg.controls.push_back( control );

  return msg.controls.back();
}

bool findCallback(handle_detector::FindHandles::Request& request, handle_detector::FindHandles::Response& response)
{
  ROS_INFO("handle_detector: Looking for handles around: %f %f %f", around_point.x, around_point.y, around_point.z);
  has_been_requested = true;
  g_should_publish = true;
  return true;
}

bool shutdownTopics(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
  ROS_INFO("handle_detector: shutting down topics");
  g_should_publish = false;
  return true;
}

void processFeedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback )
{
  std::ostringstream mouse_point_ss;
  if( feedback->mouse_point_valid )
  {
    mouse_point_ss << " at " << feedback->mouse_point.x
        << ", " << feedback->mouse_point.y
        << ", " << feedback->mouse_point.z
        << " in frame " << feedback->header.frame_id;
  }

  switch ( feedback->event_type )
  {
    case visualization_msgs::InteractiveMarkerFeedback::BUTTON_CLICK:
    {
      around_point.x = feedback->mouse_point.x;
      around_point.y = feedback->mouse_point.y;
      around_point.z = feedback->mouse_point.z;

      if(ros::service::exists("handle_detector/find_handles", false))
      {
        handle_detector::FindHandles::Request req;
        req.type = handle_detector::FindHandles::Request::DEFAULT;
        handle_detector::FindHandles::Response res;
        ros::service::call("handle_detector/find_handles", req, res);
      }
      else
      {
        ROS_ERROR("find handles service could not be found");
      }

      break;
    }
  }
  server->applyChanges();
}

void make6DofMarker( bool fixed )
{
  visualization_msgs::InteractiveMarker int_marker;
  int_marker.header.frame_id = g_workspace_frame;
  int_marker.pose.position.x = around_point.x;
  int_marker.pose.position.y = around_point.y;
  int_marker.pose.position.z = around_point.z;
  int_marker.scale = scale;
  int_marker.name = "workspace";
  int_marker.description = "workspace";

  visualization_msgs::InteractiveMarkerControl control;
  control.orientation_mode = visualization_msgs::InteractiveMarkerControl::FIXED;

  control.orientation.w = 1;
  control.orientation.x = 1;
  control.orientation.y = 0;
  control.orientation.z = 0;
  control.name = "move_x";
  control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  control.orientation.w = 1;
  control.orientation.x = 0;
  control.orientation.y = 1;
  control.orientation.z = 0;
  control.name = "move_z";
  control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  control.orientation.w = 1;
  control.orientation.x = 0;
  control.orientation.y = 0;
  control.orientation.z = 1;
  control.name = "move_y";
  control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(control);

  control.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;
  control.name = "button_control";
  visualization_msgs::Marker marker = makeBox( int_marker );
  control.markers.push_back( marker );
  control.always_visible = true;
  int_marker.controls.push_back(control);

  server->insert(int_marker);
  server->setCallback(int_marker.name, &processFeedback);
}

int main(int argc, char** argv)
{       
  // initialize random seed
  srand (time(NULL));

  // initialize ROS
  ros::init(argc, argv, "handle_detector");
  ros::NodeHandle node("~");

  // set point cloud source from launch file
  int point_cloud_source;
  node.param("point_cloud_source", point_cloud_source, 0);
  printf("point cloud source: %i\n", point_cloud_source);

  // set point cloud update interval from launch file
  node.param("sensor_topic", g_sensor_topic, std::string(RANGE_SENSOR_TOPIC));
  node.param("sensor_frame", g_sensor_frame, std::string(RANGE_SENSOR_FRAME));
  node.param("workspace_frame", g_workspace_frame, std::string(WORKSPACE_FRAME));

  double x, y, z;
  node.param("workspace_x", x, 0.0);
  node.param("workspace_y", y, 0.0);
  node.param("workspace_z", z, 0.0);

  around_point.x = x;
  around_point.y = y;
  around_point.z = z;

  ROS_INFO_STREAM("around point" << around_point.x << around_point.y << around_point.z);

  // read other parameters
  g_affordances.initParams(node);

  ros::Subscriber sub;

  // TODO: Workspace should be a cylinder around front of robot, not a box

  // interactive marker to set workspace - not needed when controlled through UI
  node.param("workspace_max_x", scale, 1.0);
  scale = 2.0*scale;
  server.reset( new interactive_markers::InteractiveMarkerServer("/handle_detector","", true) );
  ros::Duration(0.1).sleep();
  make6DofMarker( true );
  server->applyChanges();

  printf("Listening to topic: %s\n", g_sensor_topic.c_str());
  sub = node.subscribe(g_sensor_topic, 2, cloudCallback);

  ros::ServiceServer find_handles_service = node.advertiseService("find_handles", findCallback);

  // visualization of grasp affordances, and handles
  Visualizer visualizer(g_update_interval);
  ros::Publisher cylinder_pub = node.advertise<handle_detector::CylinderArrayMsg>("cylinder_list", 10);
  ros::Publisher handles_pub = node.advertise<handle_detector::HandleListMsg>("handle_list", 10);
  std::vector<visualization_msgs::MarkerArray> marker_arrays;
  visualization_msgs::MarkerArray marker_array_msg;
  visualization_msgs::MarkerArray marker_array_msg_handles;

  // visualization of point cloud
  ros::Publisher pcl_pub = node.advertise<sensor_msgs::PointCloud2>("point_cloud", 10);
  sensor_msgs::PointCloud2 pc2msg;
  PointCloud::Ptr cloud_vis(new PointCloud);
  pcl::toROSMsg(*cloud_vis, pc2msg);
  pc2msg.header.stamp = ros::Time::now();
  pc2msg.header.frame_id = WORKSPACE_FRAME;

  // publication of grasp affordances and handles as ROS topics
  Messages messages;
  ros::Publisher marker_array_pub = node.advertise<visualization_msgs::MarkerArray>("visualization_all_affordances", 10);
  ros::Publisher marker_array_pub_handles = node.advertise<visualization_msgs::MarkerArray>("visualization_all_handles", 10);
  std::vector<ros::Publisher> handle_pubs;
  handle_detector::CylinderArrayMsg cylinder_list_msg;
  handle_detector::HandleListMsg handle_list_msg;

  ros::Rate rate(1);
  while (ros::ok())
  {
    if (has_been_requested)
    {
      // reset handle list
      handle_detector::HandleListMsg msg;
      handle_list_msg = msg;
      ROS_INFO("handle_detector: Reading pointcloud...");
    }

    if (g_has_read)
    {
      // create visual point cloud
      cloud_vis = g_cloud;
      ROS_INFO("handle_detector: Updating cloud");

      // create cylinder messages for visualization and ROS topic
      marker_array_msg = visualizer.createCylinders(g_cylindrical_shells, g_workspace_frame);
      cylinder_list_msg = messages.createCylinderArray(g_cylindrical_shells, g_workspace_frame);
      ROS_INFO("handle_detector: Updating visualization");

      // create handle messages for visualization and ROS topic
      handle_list_msg = messages.createHandleList(g_handles2, g_workspace_frame);
      visualizer.createHandles(g_handles2, g_workspace_frame, marker_arrays, marker_array_msg_handles);
      handle_pubs.resize(g_handles2.size());
      for (unsigned int i=0; i < handle_pubs.size(); i++)
        handle_pubs[i] = node.advertise<visualization_msgs::MarkerArray>("visualization_handle_" + boost::lexical_cast<std::string>(i), 10);
      ROS_INFO("handle_detector: Updating messages");

      // create point cloud message
      pcl::toROSMsg(*cloud_vis, pc2msg);
      pc2msg.header.stamp = ros::Time::now();
      pc2msg.header.frame_id = g_workspace_frame;

      g_has_read = false;
    }

    if(g_should_publish)
    {
      // publish handles as ROS topic
      handles_pub.publish(handle_list_msg);

      /////////////////////////////////////////////////////////////////
      //
      // The following topics are just for debugging and visualization
      //
      /////////////////////////////////////////////////////////////////

      // publish point cloud
      pcl_pub.publish(pc2msg);

      // publish cylinders for visualization
      marker_array_pub.publish(marker_array_msg);

      // publish handles for visualization
      for (unsigned int i=0; i < handle_pubs.size(); i++)
        handle_pubs[i].publish(marker_arrays[i]);

      // publish handles for visualization
      marker_array_pub_handles.publish(marker_array_msg_handles);

      // publish cylinders as ROS topic
      cylinder_pub.publish(cylinder_list_msg);
      /////////////////////////////////////////////////////////////////
    }

    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
