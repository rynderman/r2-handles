#!/usr/bin/env python
import argparse
import rospy
import roslib; roslib.load_manifest("nasa_robot_teleop")
import threading
import tf
import numpy as np
import sensor_msgs
import geometry_msgs
from visualization_msgs.msg._InteractiveMarkerControl import InteractiveMarkerControl
from nasa_robot_teleop.marker_helper import make6DOFControls
from nasa_robot_teleop.pose_update_thread import PoseUpdateThread
from interactive_markers.interactive_marker_server import InteractiveMarkerServer
from nasa_robot_teleop.moveit_interface import MoveItInterface
from visualization_msgs.msg._InteractiveMarker import InteractiveMarker
from interactive_markers.menu_handler import MenuHandler
from visualization_msgs.msg._InteractiveMarkerFeedback import InteractiveMarkerFeedback
from handle_detector.msg import HandleListMsg
from visualization_msgs.msg._Marker import Marker
from sensor_msgs.msg._JointState import JointState
from std_msgs.msg import ColorRGBA

# Ready poses above table in sim
L_R = ['r2/left_arm/hand/index/joint0', 'r2/left_arm/hand/index/joint1', 'r2/left_arm/hand/index/joint2', 'r2/left_arm/hand/index/joint3', 'r2/left_arm/hand/little/joint0', 'r2/left_arm/hand/little/joint1', 'r2/left_arm/hand/little/joint2', 'r2/left_arm/hand/middle/joint0', 'r2/left_arm/hand/middle/joint1', 'r2/left_arm/hand/middle/joint2', 'r2/left_arm/hand/middle/joint3', 'r2/left_arm/hand/ring/joint0', 'r2/left_arm/hand/ring/joint1', 'r2/left_arm/hand/ring/joint2', 'r2/left_arm/hand/thumb/joint0', 'r2/left_arm/hand/thumb/joint1', 'r2/left_arm/hand/thumb/joint2', 'r2/left_arm/hand/thumb/joint3', 'r2/left_arm/joint0', 'r2/left_arm/joint1', 'r2/left_arm/joint2', 'r2/left_arm/joint3', 'r2/left_arm/joint4', 'r2/left_arm/joint5', 'r2/left_arm/joint6']
R_R = ['r2/right_arm/hand/index/joint0', 'r2/right_arm/hand/index/joint1', 'r2/right_arm/hand/index/joint2', 'r2/right_arm/hand/index/joint3', 'r2/right_arm/hand/little/joint0', 'r2/right_arm/hand/little/joint1', 'r2/right_arm/hand/little/joint2', 'r2/right_arm/hand/middle/joint0', 'r2/right_arm/hand/middle/joint1', 'r2/right_arm/hand/middle/joint2', 'r2/right_arm/hand/middle/joint3', 'r2/right_arm/hand/ring/joint0', 'r2/right_arm/hand/ring/joint1', 'r2/right_arm/hand/ring/joint2', 'r2/right_arm/hand/thumb/joint0', 'r2/right_arm/hand/thumb/joint1', 'r2/right_arm/hand/thumb/joint2', 'r2/right_arm/hand/thumb/joint3', 'r2/right_arm/joint0', 'r2/right_arm/joint1', 'r2/right_arm/joint2', 'r2/right_arm/joint3', 'r2/right_arm/joint4', 'r2/right_arm/joint5', 'r2/right_arm/joint6'] 
RIGHT_READY = [0.1250674816747228, 0.014078494232736993, 0.005491290506522972, 0.0015780632452822019, 0.01590840795259396, 0.006189136307118481, 0.001771481008864484, 0.12442036079865204, 0.013855012188688676, 0.00539794107614, 0.0015519928491976032, 0.013865813731484877, 0.005402250410040388, 0.0015469198578452747, -7.582762498792306e-06, 0.023614818261372683, 0.010091557753642455, 0.003015306491449543, 0.4181562804389758, -0.5960194487901092, 1.5936365814391973, -2.0373893883593057, -0.8193049096625824, 0.32133818111092616, 0.7850036568737266]
LEFT_READY = [-0.002384670220937579, 0.0005407127644652121, 0.00021118089292571085, 6.070740753916226e-05, 0.0005963423255241906, 0.00023298817285866136, 6.691083819809762e-05, -0.00243116191497883, 0.000540723220908923, 0.00021112942287793146, 6.0684631504770437e-05, 0.0005405652475367617, 0.00021115233693702606, 6.0651809547174196e-05, -0.0005798265996412866, 0.0002632664191484224, 0.00031141325349537397, 9.197516407599693e-05, -0.4423724738500425, -0.4149392207294298, -1.508064902546793, -2.0300383244466325, 0.6357353884973964, 0.32970546060613604, -0.5817032409182534]

MOVE_FRAME = "world"
RIGHT_FRAME = "r2/right_palm"
LEFT_FRAME = "r2/left_palm"
CHEST_FRAME = "r2/waist_center"

MENU_RESET, MENU_SIDE, MENU_R, MENU_L, MENU_C, MENU_PLAN, MENU_ALL = "Reset", "Set side", "Right", "Left", "Closest", "Plan first", "Recalculate all on set"

# transform from where a handle goes in hand to r2/side_palm
X_PALM_OFFSET, Y_PALM_OFFSET, Z_PALM_OFFSET = -0.08, 0.0, -0.04
# transform from pre pose to where a handle goes in hand
X_PRE_OFFSET, Y_PRE_OFFSET, Z_PRE_OFFSET = -0.2, 0.0, -0.1
# +x = further forward from wrist, +z = further out perpendicular to palm

# read in from menu to add planning steps to each move
PRE_REACH, REACH, GRASP, RESET = range(4)
RIGHT, LEFT, CLOSEST = range(3)
    
class RobotHandles(threading.Thread) :

    def __init__(self, robot_name, config_package, manipulator_group_names, joint_group_names):
        super(RobotHandles,self).__init__()

        self.robot_name = robot_name
        self.manipulator_group_names = manipulator_group_names
        self.joint_group_names = joint_group_names
        self.group_names = manipulator_group_names + joint_group_names
        self.tf_listener = tf.TransformListener()
        self.joint_data = sensor_msgs.msg.JointState()

        self.group_pose_data = {}
        self.control_frames = {}
        self.stored_poses = {}
        self.group_menu_handles = {}
        self.pose_update_thread = {}
        self.pose_store = {}
        self.auto_execute = {}

        self.is_ready_for_handles = False

        # handle markers
        self.menu_entry_names = [MENU_RESET, MENU_SIDE, MENU_R, MENU_L, MENU_C, MENU_PLAN, MENU_ALL]
        self.menu_entries = {}
        self.handle_menus = {}
        self.handle_markers = {}
        self.handles_sub = rospy.Subscriber("/handle_detector/handle_list", HandleListMsg, self.handles_callback)
        self.state = PRE_REACH
        self.plan_first_checked = False
        self.all_same_side = False
        self.side_for_all = CLOSEST # only used if all same side is used
        self.plan = False # if menu says plan, it will
        self.current_pose = geometry_msgs.msg.PoseStamped()
        
        # interactive marker server
        self.server = InteractiveMarkerServer(str(self.robot_name + "_handles"))
        rospy.Subscriber(str(self.robot_name + "/joint_states"), sensor_msgs.msg.JointState, self.joint_state_callback)

        # set up MoveIt! interface
        if config_package=="" :
            config_package =  str(self.robot_name + "_moveit_config")

        self.moveit_interface = MoveItInterface(self.robot_name,config_package)
        self.root_frame = self.moveit_interface.get_planning_frame()

        # add user specified groups
        for n in self.manipulator_group_names :
            self.moveit_interface.add_group(n, group_type="manipulator")
        for n in self.joint_group_names :
            self.moveit_interface.add_group(n, group_type="joint")

        # append group list with auto-found end effectors
        for n in self.moveit_interface.get_end_effector_names() :
            self.group_names.append(n)

        # set the control frames for all types of groups
        for n in self.group_names :
            self.control_frames[n] = self.moveit_interface.get_control_frame(n)

        # what do we have?
        self.moveit_interface.print_basic_info()

        # get stored poses from model
        for group in self.group_names :
            self.stored_poses[group] = {}
            for state_name in self.moveit_interface.get_stored_state_list(group) :
                self.stored_poses[group][state_name] = self.moveit_interface.get_stored_group_state(group, state_name)
                
        # start update threads for manipulators
        for n in self.manipulator_group_names :
            self.start_pose_update_thread(n)

        # do not move the arm automatically on the real robot     
        # self.reset_arm(LEFT)
        # self.reset_arm(RIGHT)

        self.is_ready_for_handles = True
        print "robot_handles is_ready_for_handles for handles"
        
    def keys_for_side(self, side):
        thumb = "Right Hand Thumb Open"
        hand = "right_hand"
        close_action = "Right Hand Close"
        open_action = "Right Hand Open"
        group = "right_arm"
        reset = "Right Arm High Ready Pose"
        if side == LEFT:
            group = "left_arm"
            hand = "left_hand"
            close_action = "Left Hand Close"
            open_action = "Left Hand Open"
            thumb = "Left Hand Thumb Open"
            reset = "Left Arm High Ready Pose"

        return group, hand, thumb, open_action, close_action, reset
    
    def handles_callback(self, data):
        if self.is_ready_for_handles:  
            if len(data.handles) > 0:
                self.is_ready_for_handles = False          
                print "received", str(len(data.handles)), "handles"
                self.create_handle_markers(data.handles, data.header.frame_id)
    
    # ------------------
    # Marker creation
    # ------------------
    
    def create_handle_markers(self, handles, frame) :
        i = 0
        for handle in handles :            
            pose, side = self.pose_for_handle(handle, frame)
            self.create_handle_marker(i, pose, side)
            i = i+1
        self.server.applyChanges()
        
    def add_menu(self, marker_name):
        menu_handler = MenuHandler()
        
        entry = None
        for name in self.menu_entry_names:
            if name is MENU_RESET:
                entry = menu_handler.insert(MENU_RESET, callback=self.reset_callback)
            elif name is MENU_SIDE:
                entry = menu_handler.insert(MENU_SIDE)
            elif name is MENU_R:
                entry = menu_handler.insert(MENU_R, parent=self.menu_entries[MENU_SIDE], callback=self.side_callback)
            elif name is MENU_L:
                entry = menu_handler.insert(MENU_L, parent=self.menu_entries[MENU_SIDE], callback=self.side_callback)
            elif name is MENU_C:
                entry = menu_handler.insert(MENU_C, parent=self.menu_entries[MENU_SIDE], callback=self.side_callback)
            elif name is MENU_PLAN:
                entry = menu_handler.insert(MENU_PLAN, callback=self.check_all_callback)
                menu_handler.setCheckState(entry, MenuHandler.CHECKED)
            elif name is MENU_ALL:
                entry = menu_handler.insert(MENU_ALL, callback=self.check_all_callback)
                if self.all_same_side:
                    menu_handler.setCheckState(entry, MenuHandler.CHECKED)
                else:
                    menu_handler.setCheckState(entry, MenuHandler.UNCHECKED)
                    
            self.menu_entries[name] = entry
        menu_handler.apply(self.server, marker_name)
        self.handle_menus[marker_name] = menu_handler

    def create_handle_marker(self, i, pose, side):
        marker_name = str(i)
        control = self.create_button_control(pose, side)        
        self.handle_markers[marker_name] = InteractiveMarker()
        self.handle_markers[marker_name].name = marker_name
        self.handle_markers[marker_name].controls = make6DOFControls()
        self.handle_markers[marker_name].header = pose.header
        self.handle_markers[marker_name].pose = pose.pose
        self.handle_markers[marker_name].scale = 0.2
        self.handle_markers[marker_name].controls.append(control)
        self.server.insert(self.handle_markers[marker_name], self.process_feedback)
        self.add_menu(marker_name)
    
    def color_for_side(self, side):
        if side== RIGHT:
            return ColorRGBA(1, 0, 0, 1.0)
        return ColorRGBA(0, 0, 1, 1.0)

    def create_cylinder_marker(self, pose, side):
        marker = Marker()
        marker.type = Marker.SPHERE
        marker.header.frame_id = pose.header.frame_id
        marker.action = Marker.ADD
        marker.color = self.color_for_side(side)
        marker.scale.x = 0.05
        marker.scale.y = 0.15
        marker.scale.z = 0.05
        marker.pose = pose.pose
        return marker

    def create_button_control(self, pose, side):
        marker = self.create_cylinder_marker(pose, side)
        control = InteractiveMarkerControl()
        control.always_visible = True
        control.interaction_mode = InteractiveMarkerControl.BUTTON
        control.markers.append(marker)
        control.name = "button"
        return control
                
    def update_color(self, pt, marker_name):
    # could just save all markers in their own dictionary, name them the index
        for control in self.handle_markers[marker_name].controls:
            if control.name == "button":
                side = self.side_for_all
                if self.side_for_all is CLOSEST:
                    hand_frame, side = self.frame_and_side_for_pose(pt)
                control.markers[0].color = self.color_for_side(side)
                control.markers[0].pose = pt.pose
                self.server.insert(self.handle_markers[marker_name], self.process_feedback)
    
    # ------------------
    # Handles to pose calculation
    # ------------------
    
    def calculate_mean_handle(self, sensor_frame, handle):
        x, y, z = 0.0, 0.0, 0.0
        avg = geometry_msgs.msg.PointStamped()
        avg.header.frame_id = sensor_frame
        avg.header.stamp = rospy.Time(0)

        count = len(handle.cylinders)
        for cyl in handle.cylinders:
            x += cyl.pose.position.x
            y += cyl.pose.position.y
            z += cyl.pose.position.z  
        x = x / count
        y = y / count
        z = z / count  
        avg.point.x = x
        avg.point.y = y
        avg.point.z = z

        # pick right or left hand
        hand_frame = LEFT_FRAME
        side = LEFT
                
        if not self.all_same_side or self.side_for_all is CLOSEST:
            pose = geometry_msgs.msg.PoseStamped()
            pose.header = avg.header
            pose.pose.position = avg.point
            hand_frame, side = self.frame_and_side_for_pose(pose)
        else:
            side = self.side_for_all
            if side is RIGHT: 
                hand_frame = RIGHT_FRAME

        dist = 1000.0
        closest = handle.cylinders[0]

        for cyl in handle.cylinders:
            distance = np.sqrt( pow((x - cyl.pose.position.x), 2) + 
                                pow((y - cyl.pose.position.y), 2) +
                                pow((z - cyl.pose.position.z), 2) )
            if distance < dist:
                dist = distance
                closest = cyl
                
        vec3 = geometry_msgs.msg.Vector3Stamped()
        vec3.vector = closest.axis
        vec3.header = avg.header
        axis = self.tf_listener.transformVector3(hand_frame, vec3)
        
        closest_point = geometry_msgs.msg.PointStamped()
        closest_point.header = avg.header
        closest_point.point = closest.pose.position
        closest_point = self.tf_listener.transformPoint(hand_frame, closest_point)        
        return [axis.vector.x, axis.vector.y, axis.vector.z], closest_point, hand_frame, side
    
    def pose_for_major_axis(self, major, pt, frame):
        pose = geometry_msgs.msg.PoseStamped()
        # vector from handle to hand in left_arm frame
        vector = tf.transformations.unit_vector([-pt.point.x, -pt.point.y, -pt.point.z])
        # vector in direction of arm
        perp = np.cross(major, vector)
        # vector perpendicular to arm vector and major axis
        perp2 = np.cross(major, perp)
        M = np.array(((perp2[0], major[0], perp[0], 0), 
                      (perp2[1], major[1], perp[1], 0), 
                      (perp2[2], major[2], perp[2], 0), 
                      (0, 0, 0, 1)), dtype=np.float64)
        
        x, y, z, w = tf.transformations.quaternion_from_matrix(M)
        pose.header.stamp = rospy.Time(0)
        pose.header.frame_id = frame
        pose.pose.position = pt.point
        pose.pose.orientation.x = x
        pose.pose.orientation.y = y
        pose.pose.orientation.z = z
        pose.pose.orientation.w = w
        return pose

    def pose_for_handle(self, handle, sensor_frame):
        pose = geometry_msgs.msg.PoseStamped()
        
        # cylinder major axis and location in hand_frame frame
        major_1, pt, hand_frame, side = self.calculate_mean_handle(sensor_frame, handle)
        
        # major axis facing the other way
        major_2 = [-major_1[0], -major_1[1], -major_1[2]] 
        pose_1 = self.pose_for_major_axis(major_1, pt, hand_frame)
        pose_2 = self.pose_for_major_axis(major_2, pt, hand_frame) 
        
        # This gets the roll pitch and yaw from the pose about fixed axes X, Y, Z respectively.
        r_1, p_1, y_1 = tf.transformations.euler_from_quaternion([pose_1.pose.orientation.x, pose_1.pose.orientation.y, 
                pose_1.pose.orientation.z, 
                pose_1.pose.orientation.w])
        r_2, p_2, y_2 = tf.transformations.euler_from_quaternion([pose_2.pose.orientation.x, 
                pose_2.pose.orientation.y, 
                pose_2.pose.orientation.z, 
                pose_2.pose.orientation.w])
        
        # pick based least rotation around x axis.
        if np.abs(r_1) <= np.abs(r_2):
            pose = pose_1
            self.handle_pose_2 = pose_2
        else:
            pose = pose_2
            self.handle_pose_2 = pose_1
        self.tf_listener.waitForTransform(pose.header.frame_id, MOVE_FRAME, rospy.Time(0), rospy.Duration(10.0))
        pose = self.tf_listener.transformPose(MOVE_FRAME, pose)
        return pose, side
    
    # ------------------
    # System
    # ------------------
    
    def start_pose_update_thread(self, group):
        self.group_pose_data[group] = geometry_msgs.msg.PoseStamped()
        try :
            self.pose_update_thread[group] = PoseUpdateThread(group, self.root_frame, self.control_frames[group], self.tf_listener, None)
            self.pose_update_thread[group].start()
        except :
            rospy.logerr("RobotHandles::start_pose_update_thread() -- unable to start group update thread")

    def joint_state_callback(self, data):
        self.joint_data = data

    # ------------------
    # Planning
    # ------------------
    
    def frame_and_side_for_pose(self, pt):
        self.tf_listener.waitForTransform(pt.header.frame_id, CHEST_FRAME, rospy.Time(0), rospy.Duration(10.0))
        chest = self.tf_listener.transformPose(CHEST_FRAME, pt)
        side = LEFT
        hand_frame = LEFT_FRAME
        if chest.pose.position.y >= 0:
            side = RIGHT
            hand_frame = RIGHT_FRAME
        return hand_frame, side

    def side_and_poses_with_offsets(self, pt, x, y, z):
        hand_frame, side = self.frame_and_side_for_pose(pt)
            
        self.tf_listener.waitForTransform(pt.header.frame_id, hand_frame, rospy.Time(0), rospy.Duration(10.0))
        hand_frame_pose = self.tf_listener.transformPose(hand_frame, pt)
        pre_pose = self.tf_listener.transformPose(hand_frame, pt)
        
        hand_frame_pose.pose.position.x = (hand_frame_pose.pose.position.x + x)
        hand_frame_pose.pose.position.z = (hand_frame_pose.pose.position.z + z)
        pre_pose.pose.position.x = (hand_frame_pose.pose.position.x + X_PRE_OFFSET)
        pre_pose.pose.position.z = (hand_frame_pose.pose.position.z + Z_PRE_OFFSET) 
        
        self.tf_listener.waitForTransform(hand_frame, pt.header.frame_id, rospy.Time(0), rospy.Duration(10.0))
        pt = self.tf_listener.transformPose(pt.header.frame_id, hand_frame_pose)
        pre_pose = self.tf_listener.transformPose(pre_pose.header.frame_id, pre_pose)

        return side, pt, pre_pose
            
    def has_moved(self, pt):
        return not (pt.pose.position.x == self.current_pose.pose.position.x and
                    pt.pose.position.y == self.current_pose.pose.position.y and
                    pt.pose.position.z == self.current_pose.pose.position.z and
                    pt.pose.orientation.x == self.current_pose.pose.orientation.x and
                    pt.pose.orientation.y == self.current_pose.pose.orientation.y and
                    pt.pose.orientation.z == self.current_pose.pose.orientation.z and
                    pt.pose.orientation.w == self.current_pose.pose.orientation.w)

    def save_current_pose(self, pt):
        self.current_pose.pose.position.x = pt.pose.position.x
        self.current_pose.pose.position.y = pt.pose.position.y
        self.current_pose.pose.position.z = pt.pose.position.z
        self.current_pose.pose.orientation.x = pt.pose.orientation.x 
        self.current_pose.pose.orientation.y = pt.pose.orientation.y 
        self.current_pose.pose.orientation.z = pt.pose.orientation.z 
        self.current_pose.pose.orientation.w = pt.pose.orientation.w 

    # ------------------
    # Control
    # ------------------
    
    def reset_arm(self, side):
        js = JointState()
        js.name = L_R
        js.position = LEFT_READY
        if side == RIGHT:
            js.name = R_R
            js.position = RIGHT_READY
        arm, hand, thumb, open_action, close_action, reset = self.keys_for_side(side)

        self.moveit_interface.set_display_mode(arm, "last_point")

        self.moveit_interface.groups[arm].clear_pose_targets()
        # uses joints that work in sim
        #self.moveit_interface.create_joint_plan_to_target(arm, js)
        # should use ready pose high
        self.moveit_interface.create_joint_plan_to_target(arm, self.stored_poses[arm][reset])

        r = self.moveit_interface.execute_plan(arm)
        if not r : rospy.logerr(str("RobotHandles: failed moveit execution for group: " + arm))
        
        # open hand
        self.moveit_interface.create_joint_plan_to_target(hand, self.stored_poses[hand][open_action])
        r = self.moveit_interface.execute_plan(hand)
        if not r : rospy.logerr(str("RobotHandles: failed moveit execution for group: " + hand))
        
    def set_hand(self, hand, action, plan):
        self.moveit_interface.create_joint_plan_to_target(hand, self.stored_poses[hand][action])
        if not plan:
            r = self.moveit_interface.execute_plan(hand)
            if not r: rospy.logerr(str("RobotHandles: failed moveit execution for group: " + hand))
            return r

    def set_for_group(self, pt, group, plan):
        self.moveit_interface.groups[group].clear_pose_targets()
        self.moveit_interface.create_plan_to_target(group, pt)
        self.moveit_interface.set_display_mode(group, "last_point")
        if not plan:
            r = self.moveit_interface.execute_plan(group)
            if not r: rospy.logerr(str("RobotHandles: failed moveit execution for group: " + group))
            return r

    def process(self, pt, side, pre_pt):       
        
        # check to see if planning first
        if self.plan_first_checked:
            # if planning first toggle should plan
            self.plan = not self.plan
        else:
            self.plan = False
        
        group, hand, thumb, open_action, close_action, reset = self.keys_for_side(side)
        
        if self.state == PRE_REACH: 
            # pre position thumb
            self.set_hand(hand, thumb, self.plan) 
            # move to pre
            self.set_for_group(pre_pt, group, self.plan)
            if not self.plan: 
                self.state = REACH
                
        elif self.state == REACH: 
            # move to final
            self.set_for_group(pt, group, self.plan)
            if not self.plan:
                self.state = GRASP
                
        elif self.state == GRASP: 
            # close hand
            self.set_hand(hand, close_action, self.plan)
            if not self.plan: 
                self.state = RESET
                
        elif self.state == RESET:
            self.reset_arm(side)
            self.state = PRE_REACH
            self.plan = False
            self.is_ready_for_handles = True
    
    # ------------------
    # Feedback
    # ------------------
    def side_callback(self, feedback):
        if feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
            side = self.menu_entry_names[feedback.menu_entry_id - 1]
            if side is MENU_R:
                self.side_for_all = RIGHT
            elif side is MENU_L:
                self.side_for_all = LEFT
            else:
                self.side_for_all = CLOSEST

            if self.all_same_side:
                # recalculate all poses for selected arm arm
                self.is_ready_for_handles = True
            else:                
                # show moved handles for selected arm, move selected arm
                pt = geometry_msgs.msg.PoseStamped()
                pt.header = feedback.header
                pt.pose = feedback.pose
                self.update_color(pt, feedback.marker_name)
                self.server.applyChanges()
            
    def reset_callback(self, feedback):
        if feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
            # TODO: figure out whch arm to reset
            pt = geometry_msgs.msg.PoseStamped()
            pt.header = feedback.header
            pt.pose = feedback.pose
            self.plan = False
            hand_frame, side = self.frame_and_side_for_pose(pt)
            if self.side_for_all is CLOSEST:
                self.reset_arm(side)
            else:
                self.reset_arm(self.side_for_all)

    def check_all_callback(self, feedback):
        if feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT:
            marker = feedback.marker_name
            menu_entry = feedback.menu_entry_id
            state = self.handle_menus[marker].getCheckState( menu_entry )
            
            # set all markers to new check state            
            if state == MenuHandler.CHECKED:
                for marker_name,menu in self.handle_menus.iteritems() :
                    menu.setCheckState( menu_entry, MenuHandler.UNCHECKED )
                    menu.reApply( self.server )
                if self.menu_entry_names[menu_entry-1] is MENU_PLAN:
                    self.plan_first_checked = False
                else:
                    self.all_same_side = False
                    self.side_for_all = CLOSEST
            else:
                for marker_name,menu in self.handle_menus.iteritems() :
                    menu.setCheckState( menu_entry, MenuHandler.CHECKED )
                    menu.reApply( self.server )
                if self.menu_entry_names[menu_entry-1] is MENU_PLAN:
                    self.plan_first_checked = True
                else:
                    self.all_same_side = True

            self.server.applyChanges()
            
    def process_feedback(self, feedback):
        if feedback.event_type == InteractiveMarkerFeedback.MOUSE_UP:
            pt = geometry_msgs.msg.PoseStamped()
            pt.header = feedback.header
            pt.pose = feedback.pose
            
            # if moved, make sure it has the right color
            # and go back to pre reach
            if self.has_moved(pt):  
                self.plan = False
                self.state = PRE_REACH
                self.update_color(pt, feedback.marker_name)

        elif feedback.event_type == InteractiveMarkerFeedback.BUTTON_CLICK:
            pt = geometry_msgs.msg.PoseStamped()
            pt.header = feedback.header
            pt.pose = feedback.pose
            self.save_current_pose(pt)
            side, pt, pre_pt = self.side_and_poses_with_offsets(pt, X_PALM_OFFSET, Y_PALM_OFFSET, Z_PALM_OFFSET)
            if self.side_for_all is CLOSEST:
                self.process(pt, side, pre_pt)
            else:
                self.process(pt, self.side_for_all, pre_pt)

            
        self.server.applyChanges()

if __name__=="__main__":
    parser = argparse.ArgumentParser(description='Robot Handles')
    parser.add_argument('-r, --robot', dest='robot', help='e.g. r2')
    parser.add_argument('-c, --config', dest='config', help='e.g. r2_fullbody_moveit_config')
    parser.add_argument('-m, --manipulatorgroups', nargs="*", dest='manipulatorgroups', help='space delimited string e.g. "left_arm left_leg right_arm right_leg"')
    parser.add_argument('-j, --jointgroups', nargs="*", dest='jointgroups', help='space limited string e.g. "head waist"')
    parser.add_argument('positional', nargs='*')
    args = parser.parse_args()

    rospy.init_node("RobotHandles")

    try:
        robot = RobotHandles(args.robot, args.config, args.manipulatorgroups, args.jointgroups)
        robot.start()
    except rospy.ROSInterruptException:
        # TODO: should not pass
        pass

    r = rospy.Rate(50.0)
    while not rospy.is_shutdown():
        r.sleep()
